"use strict";

Template.posts.helpers({
	postList: function () {
		return Collections.Post.find({});
	},

	Image: function () {
		var images = Collections.Images.find({
			_id: this.ImageId
		}).fetch();

		return images[0].url();


	},

	updateMasonry: function () {
		$('.grid').imagesLoaded().done(function () {
			$('.grid').masonry({
				itemSelector: '.grid-item',
				columnWidth: '.grid-sizer',
				gutter: 20,
				percentPosition: true
			});
		});
	}
});