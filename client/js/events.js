"use strict";

Template.form.events({
	'submit .add-new-post': function (event) {
		event.preventDefault();

		var postImage = event.currentTarget.children[0].files[0];

		var name = event.currentTarget.children[1].value;

		var message = event.currentTarget.children[2].value;

		if (name === "") {
			Materialize.toast('Must put a name', 4000);
			return false;
		}

		if (message === "") {
			Materialize.toast('Must put a message', 4000);
			return false;
		}

		if (postImage === undefined) {
			Materialize.toast('Must post an image', 4000);
			return false;
		}

		Collections.Images.insert(postImage, function (error, fileObject) {
			if (error) {

			} else {
				Collections.Post.insert({
					name: name,
					createdAt: new Date(),
					message: message,
					ImageId: fileObject._id
				});
				$('.grid').masonry('reloadItems');
			}
		});
	}
});